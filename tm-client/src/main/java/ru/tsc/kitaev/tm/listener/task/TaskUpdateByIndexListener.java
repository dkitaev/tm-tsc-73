package ru.tsc.kitaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIndexListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "task-update-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by index...";
    }

    @Override
    @EventListener(condition = "@taskUpdateByIndexListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter index");
        final int index = TerminalUtil.nextNumber() - 1;
        if (!taskEndpoint.existsTaskByIndex(sessionService.getSession(), index)) throw new TaskNotFoundException();
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        @NotNull final String description = TerminalUtil.nextLine();
        taskEndpoint.updateTaskByIndex(sessionService.getSession(), index, name, description);
    }

}
