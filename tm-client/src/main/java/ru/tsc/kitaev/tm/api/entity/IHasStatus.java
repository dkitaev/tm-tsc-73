package ru.tsc.kitaev.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull Status getStatus();

    void setStatus(@NotNull Status status);

}
