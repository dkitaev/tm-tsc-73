package ru.tsc.kitaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kitaev.tm.event.ConsoleEvent;
import ru.tsc.kitaev.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "task-remove-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by index...";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter Index");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        System.out.println("[REMOVE PROJECT BY INDEX]");
        taskEndpoint.removeTaskByIndex(sessionService.getSession(), index);
        System.out.println("[OK]");
    }

}
