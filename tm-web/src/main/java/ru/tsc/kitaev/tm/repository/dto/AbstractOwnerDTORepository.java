package ru.tsc.kitaev.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.lang.Nullable;
import ru.tsc.kitaev.tm.dto.AbstractOwnerEntityDTO;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface AbstractOwnerDTORepository<E extends AbstractOwnerEntityDTO> extends AbstractDTORepository<E> {

    @Nullable
    List<E> findAllByUserId(@NotNull String userId);

    @Nullable
    Optional<E> findByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    void deleteAllByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
