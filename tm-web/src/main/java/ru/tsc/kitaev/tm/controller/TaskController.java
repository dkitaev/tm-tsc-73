package ru.tsc.kitaev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.kitaev.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kitaev.tm.api.service.dto.ITaskDTOService;
import ru.tsc.kitaev.tm.api.service.model.ITaskService;
import ru.tsc.kitaev.tm.dto.CustomUser;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.TaskDTO;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private ITaskDTOService taskDTOService;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectDTOService projectService;

    @GetMapping("/task/create")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        taskDTOService.add(user.getUserId(), new TaskDTO("New Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.deleteById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") TaskDTO task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        task.setUserId(user.getUserId());
        taskDTOService.update(task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        final TaskDTO task = taskDTOService.findById(user.getUserId(), id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects(user));
        modelAndView.addObject("statuses", getStatus());
        return modelAndView;
    }

    private Collection<ProjectDTO> getProjects(@AuthenticationPrincipal final CustomUser user) {
        return projectService.findAll(user.getUserId());
    }

    public Status[] getStatus() {
        return Status.values();
    }

}
