package ru.tsc.kitaev.tm.service.model;

import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tsc.kitaev.tm.api.service.model.IProjectService;
import ru.tsc.kitaev.tm.model.Project;

@Service
@NoArgsConstructor
public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

}
