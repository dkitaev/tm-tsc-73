package ru.tsc.kitaev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.AbstractOwnerModel;

import java.util.List;

public interface IOwnerService<E extends AbstractOwnerModel> extends IService<E> {

    @NotNull
    E add(@Nullable final String userId, @NotNull final E entity);

    @NotNull
    E update(@Nullable final String userId, @NotNull final E entity);

    @Nullable
    List<E> findAll(@Nullable final String userId);

    @Nullable
    E findById(@Nullable final String userId, @NotNull final String id);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    int getSize(@Nullable final String userId);

    void clear(@Nullable final String userId);

    void delete(@Nullable final String userId, @NotNull final E entity);

    void deleteById(@Nullable final String userId, @NotNull final String id);

}
