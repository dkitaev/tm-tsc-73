package ru.tsc.kitaev.tm.api.entity;

public interface IWBS extends IHasStartDate, IHasName, IHasFinishDate, IHasStatus {
}
