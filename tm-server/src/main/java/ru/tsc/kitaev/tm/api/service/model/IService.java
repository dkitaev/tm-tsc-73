package ru.tsc.kitaev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.model.AbstractModel;

import java.util.List;

public interface IService<E extends AbstractModel> {

    void clear();

    List<E> findAll();

    E findById(@NotNull final String id);

    void removeById(@NotNull final String id);

    void addAll(@NotNull final List<E> entities);

    int getSize();

}
